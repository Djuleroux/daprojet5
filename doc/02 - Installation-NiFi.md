Deuxième partie de notre suite d'articles sur Apache NiFi, ici nous allons parler de son installation. NiFi peut être installé de plusieurs manières, nous allons en présenter 4 :

* L'installation simple de NiFi, utile à des fins de développement par exemple,
* le déploiement de NiFi en clusters, destiné à la production,
* l'installation dans un environnement docker avec docker-compose, 
* la compilation de NiFi et/ou d'un processeur optionnel depuis les sources.

Nous présenterons ensuite quelques bonnes pratiques de configurations, notamment vis à vis des performances pour des flux rapides de données.

### Installation de NiFi

#### Installation simple, mode développeur

Dans ce mode d'installation, on va simplement s'appuyer sur Java pour lancer NiFi en mode utilisateur. 

Vérifiez tout d'abord que vous ayez une version récente de Java (8+) sur votre machine, sinon suivez les instructions d'installation de votre système.

Pour ma part je conseille la distribution libre ["Zulu Community"](https://www.azul.com/downloads/zulu-community/?&architecture=x86-64-bit&package=jdk) de java, mais l'openjdk de votre distribution linux ou encore le jdk d'Oracle sont tout aussi bien.

Pour installer NiFi, il suffit de :

1. Télécharger les fichiers binaires depuis le site de NiFi : [Nifi Download](http://nifi.apache.org/download.html),
2. Décompresser l'archive dans un répertoire de votre choix (ex : **/home/[username]/nifi**)),
3. D’exécuter `bin/nifi.sh run` dans le répertoire,
4. Se connecter à **http://localhost:8080** pour accéder à l'interface web.

Pour ajouter NiFi en tant que service :

* Exécutez `bin/nifi.sh install nifi` dans le répertoire,
* Démarrez ou arrêtez le service avec les commandes `service start nifi` ou `service stop nifi` comme pour tout service standard.

Une fois connecté à l'UI vous êtes face à l'interface de NiFi :

[![](https://dju-da.site/wp-content/uploads/2020/01/nifi-ui-1024x428.png)](https://dju-da.site/wp-content/uploads/2020/01/nifi-ui.png)
*fig 1 : Nifi UI*
&nbsp;

#### Passez NiFi en mode production (cluster)

Comme évoqué ci-dessus, un cluster NiFi nécessite un cluster **Zookeper** pour fonctionner, idéalement constitué de 3 nœuds minimum pour la sécurité et la notion de quorum (élection et validation des modifications par la majorité des nœuds).

Par chance, NiFi embarque en standard Zookeeper , mais il est tout à fait possible, et recommandé en production, d'utiliser un cluster Zookeeper existant. L'installation et la configuration d'un cluster Zookeeper est un vaste sujet, trop long à développer ici, mais la documentation est très détaillée. Vous la trouverez ici : [Zookeeper](https://zookeeper.apache.org/doc/r3.3.3/zookeeperOver.html)

Pour l'exemple, on va partir sur le principe d'un cluster de 3 nœuds (serveurs ou machines virtuelles) chacun avec les services Zookeeper et NiFi.

1. Sur chaque nœud faites l'installation comme ci-dessus.

2. Configurez le serveur DNS, ou vos fichiers **/etc/hosts** afin que chaque nœud soit joignable par son nom dns ou un alias par les autres nœuds. Par exemple zoonif-1, zoonif-2, zoonif-3 donne comme fichier host sur chacun des noeuds :

```shell
 192.168.1.11 zoonif-1
 192.168.1.12 zoonif-2
 192.168.1.13 zoonif-3
```

3. Sur chaque serveur, il faut ensuite modifier le fichier **[..]conf/zookeeper.properties** pour indiquer la liste des serveurs :

```shell
server.1=zoonif-1:2888:3888
server.2=zoonif-2:2888:3888
server.3=zoonif-3:2888:3888
```

4. Si vous le souhaitez, vous pouvez aussi modifier les variables **datadir** du même fichier pour indiquer le répertoire de stockage des données Zookeper.

5. Reste à configurer Nifi en éditant le fichier **conf/nifi.properties** sur chacune des machines. Tout d'abord on indique à NiFi qu'on utilise le Zookeeper intégré :

```shell
nifi.state.management.embedded.zookeeper.start=true
```

6. On indique ensuite la chaîne de connexion à Zookeeper:

```shell
nifi.zookeeper.connect.string=zoonif-1:2181,zoonif-2:2181,zoonif-3:2181
```

7.  Dans la partie **cluster node properties** du fichier on active le nœud puis on indique pour chaque node son nom et un port > 1024 pour la communication (le même pour chaque nœud):

```shell
nifi.cluster.is.node=true
nifi.cluster.node.address=zoonif-1 # (à adapter au nœud)
nifi.cluster.node.protocol.port= 9888
```

8. Il ne reste plus qu'à lancer Nifi sur chacune des machines, en mode utilisateur ou service puis à se connecter à un des serveurs (par exemple **http://zoonif-1:8080**)

On voit maintenant que Nifi est composé de 3 nœuds :

[![](https://dju-da.site/wp-content/uploads/2020/01/nifi-ui-cluster-1024x288.png)](https://dju-da.site/wp-content/uploads/2020/01/nifi-ui-cluster.png)
*fig 2 : Nifi UI (cluster)*
&nbsp;

On peut également accéder à l'interface de gestion des nœuds :

[![](https://dju-da.site/wp-content/uploads/2020/01/nifi-cluster-mngt.png)](https://dju-da.site/wp-content/uploads/2020/01/nifi-cluster-mngt.png)
*fig 3 : Nifi UI de gestion du cluster*
&nbsp;

Il existe plein d'autres options de configurations du cluster notamment pour sécuriser et crypter la communication entre les nœuds, utilisé HTTPS pour l'accès à l'UI, etc...

Vous trouverez le guide complet d'administration ici : [NiFi Administration Guide](https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html)
&nbsp;

#### Installation sous docker avec docker-compose

Sur le dépôt GitHub des sources de données de NiFi, Apache met à disposition [une définition pour docker-compose](https://github.com/apache/nifi/tree/master/nifi-docker/docker-compose) permettant de déployer NiFi très simplement sous docker, notamment en mode cluster.

Sur un hôte disposant de docker et docker-compose :

1. Copiez le fichier **docker-compose.yml** depuis le dépôt apache NiFi,
2. Éditez le fichier pour changer la source du docker NiFi :

```Dockerfile
version: "3"
services:
  zookeeper:
    hostname: zookeeper
    container_name: zookeeper
    image: 'bitnami/zookeeper:latest'
    environment:
      - ALLOW_ANONYMOUS_LOGIN=yes
  nifi:
    image: apache/nifi:latest
    ports:
      - 8080 # Unsecured HTTP Web Port
    environment:
      - NIFI_WEB_HTTP_PORT=8080
      - NIFI_CLUSTER_IS_NODE=true
      - NIFI_CLUSTER_NODE_PROTOCOL_PORT=8082
      - NIFI_ZK_CONNECT_STRING=zookeeper:2181
      - NIFI_ELECTION_MAX_WAIT=1 min
```
&nbsp;

3. Ensuite il suffit de démarrer votre cluster avec la commande docker-compose depuis le répertoire contenant le docker-compose.yml:
    * en mode nœud unique : `docker-compose up -d`
    * en mode cluster avec 3 nœuds: `docker-compose up --scale nifi=3 -d`

En mode cluster il faut patienter une petite minute le temps que l'élection du coordinateur se fasse.

4. une fois le cluster démarré, vérifiez les ports des instances avec `docker ps` :

```shell
sudo docker ps

CONTAINER ID        IMAGE                      COMMAND                  CREATED             STATUS              PORTS                                          NAMES
ca94a7e52b36        apache/nifi:latest          "../scripts/start.sh"    3 minutes ago       Up 3 minutes        8443/tcp, 10000/tcp, 0.0.0.0:32779->8080/tcp   nifi-compose_nifi_1
116ad3e0734b        apache/nifi:latest          "../scripts/start.sh"    3 minutes ago       Up 3 minutes        8443/tcp, 10000/tcp, 0.0.0.0:32778->8080/tcp   nifi-compose_nifi_3
dfba76bb1af9        apache/nifi:latest          "../scripts/start.sh"    3 minutes ago       Up 3 minutes        8443/tcp, 10000/tcp, 0.0.0.0:32777->8080/tcp   nifi-compose_nifi_2
52f4193ff454        bitnami/zookeeper:latest   "/app-entrypoint.sh …"   3 minutes ago       Up 3 minutes        2181/tcp, 2888/tcp, 3888/tcp                   zookeeper
```
&nbsp;

5. Il ne reste plus qu'à se connecter sur une des instances sur son adresse par exemple **http://localhost:32779/nifi/**

Vous pouvez également augmenter ou diminuer le nombre de noeuds du cluster en utilisant les commandes `docker-compose up --scale nifi=4 -d` pour passer à 4 noeuds, ou encore `docker-compose up --scale nifi=2 -d` pour passer à 2 noeuds.
&nbsp;

------------

### Compilation de NiFi et de ses composants

Depuis la version 1.10 NiFi n'intègre plus dans son package binaire certains composants (processeurs) jusque là livré en standard :
* les composants Kite, permettant notamment de manipuler les fichiers CSV
* les composants flume (traitement de fichier journaux)
* les composants pour Kafka V0.8 (gestion de file d'attente de message)
* ...

Afin de disposer de tous les composants, plusieurs solutions :
* installer le package binaire de NiFi puis compiler les composants souhaités depuis les sources du projet
* installer nifi au complet avec tous ses composants en le compilant depuis les sources
* Récupérer les fichiers compilés des composants souhaités sur un repository maven, comme par exemple [MVNRepository](https://mvnrepository.com/artifact/org.apache.nifi)

Nous allons voir ici comment compiler NiFi ou un composant depuis les sources.


#### Pré-requis :

Il faut avoir au minimum le jdk 8 (version à jour), une version récente de Git. Il faut également une version 3.2 minimum de Maven, récupérable ici : [Maven download](https://maven.apache.org/download.cgi "Maven download").

L'installation de maven se fait en dézippant les sources dans un répertoire choisi puis en éditant le **system path** pour y ajouter le sous-répertoire bin.

Notez que pour certains OS, il existe des installations binaires (Ubuntu, Debian, Windows), vérifiez juste la version du paquet/setup si votre OS est un peu ancien.


#### Récupération des sources et compilation

1. Clonez le projet git :

```shell
git clone https://gitbox.apache.org/repos/asf/nifi.git
```

2. Placez vous sur la branche et tag souhaité, par défaut sur master et dernier tag qui sont les sources de la prochaine version 1.x, par exemple pour se placer sur la release 1.1 :

```shell
# on récupère les tags
git fetch --all --tags
# on checkout le release 1.1
git checkout rel/nifi-1.11.0
```

Il ne reste plus qu'à compiler NiFi et/ou ses composants :

1. On construit le projet avec Maven en se placant à la racine des sources et en lançant la commande : `mvn -T C2.0 clean install -Pinclude-grpc`

2. Pour ne construire que les extensions ou une extension souhaitée, il suffit de construire le ou les bundles souhaités depuis leur répertoire racine puis d'éxecuter `mvn -T C2.0 clean install -Ddir-only` depuis le répertoire **nifi-assembly**

Les fichiers binaires ainsi que le package complet de NiFi sont ensuite accessibles dans le répertoire **target/releaseName**, il ne reste plus qu'à l'installer comme vu précédemment.
&nbsp;

------------

### Bonnes pratiques de configuration

Les valeurs systèmes par défaut de Linux ne sont pas forcement adaptées à une applications comme NiFi qui, au même titre qu'Apache Kafka par exemple, est intensif au niveau des entrées-sorties.

Voici quelques recommandations pour commencer :

* Maximum File Handles
NiFi peut potentiellement nécessiter de nombreux fichiers ouverts en même temps, il faut augmenter le nombre dans **/etc/security/limits.conf** :

```shell
 *  hard  nofile  50000
 *  soft  nofile  50000
```

* Maximum Forked Processes
NiFi peut également générer de nombreux threads de manière simultanée. Toujours dans **/etc/security/limits.conf** :

```shell
 *  hard  nproc  10000
 *  soft  nproc  10000
```

* Nombre de socket TCP disponible
NiFi peut pour certains flux de données ouvrir et fermer de nombreux ports très rapidement. En **shell** :

```shell
sudo sysctl -w net.ipv4.ip_local_port_range="10000 65000"
```

A ce titre, il peut aussi être utile que les sockets ouvertes ne le restent pas trop longtemps : 

```shell
sudo sysctl -w net.ipv4.netfilter.ip_conntrack_tcp_timeout_time_wait="1"
```

* Désactivation du swap
Hormis sur SSD très rapide, on veut éviter que NiFI swappe ses données sur le disque. On peut désactiver le swap en éditant **/etc/sysctl.conf** :

```shell
vm.swappiness = 0
```

* Désactivation du suivi des accès fichier sur les partitions de travail de NiFi
On peut gagner en performance en montant sur des partitions séparées les volumes de données sur lequel travaille NiFi et en désactivant le suivi des accès fichier (**atime**) pour ses partitions dans le **/etc/fstab**
&nbsp;
