Première partie d'une série de quatre articles ayant pour but de présenter la solution Apache NiFi de gestion de flux de données.

Dans cette première partie nous présenterons NiFi : son architecture, ses fonctionnalités, comment réaliser un flux simple et ses forces/faiblesses face à d'autres solutions.

Les articles suivants présenteront :
* l'installation de NiFi et sa configuration ([partie 2](https://dju-da.site/data-processing/nifi-installation-configuration/ "partie 2"))
* un cas d'usage de flux temps réel avec la gestion de tweets en temps réel ([partie 3](https://dju-da.site/data-processing/analyse-temps-reel-des-tweets-avec-nifi/ "partie 3"))
* un cas d'usage d'ingestion de données en lot, avec notamment la connexion à HDFS.([partie 4](https://dju-da.site/data-processing/creer-son-datalake-avec-nifi-et-hdfs/ "partie 4"))
&nbsp;

### Présentation

Apache NiFi est un outil de gestion de flux de données, c'est à dire qu'il permet de :

* De construire des flux de données entre systèmes différents (API, Fichiers, BDD, stream, hdfs...),
* Modifier et gérer en temps réel un ou plusieurs flux,
* Contrôler en temps réel l'état des acheminements et traitements de données effectués,
* Faire du reporting sur l'historique des flux et données traitées.

C'est un outil open source de la fondation Apache, qui a repris un projet original de la NSA, le logiciel "Niagara Files".

NiFi repose sur le principe du "flow-based programming", c'est à dire sur le principe d'un réseau d’éléments **processeurs** reliés entre eux par des flux de messages, **les liaisons**, le tout formant une application. Chaque élément processeur est un composant indépendant qui peut être réutilisé dans un ou plusieurs flux, sans changement interne.

Ses principales caractéristiques sont :

* Une interface utilisateur basée sur le web, permettant la création d'applications en glissé déposé,
* une interface unique pour la création, la modification, le contrôle et le reporting,
* le suivi des données de bout en bout,
* le fonctionnement en environnement distribué (cluster),
* l'extensibilité (les utilisateurs peuvent développer leurs propres composants),
* le cryptage des flux par TLS (Transport layer Security),
* le support des certificats clients, Kerberos, LDAP, Apache Ranger ou encore Apache Knox pour la gestion de l'authentification et des autorisations,
* la gestion modulaire des ACL : par composants, fonctionnalités, groupes, flux, etc..

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/flow-th.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/flow-th.png)
*fig 1 :Interface web Nifi*
&nbsp;

------------


### Architecture

Apache NiFi est un programme écrit en Java, une fois lancé, il s’exécute entièrement dans la machine virtuelle java (JVM) sur le ou les système(s) sur lequel il est installé.

Le fonctionnement de NiFi s'articule autours de la notion de **FlowFile**. Un FlowFile est un enregistrement de données qui consiste en :
* Un pointeur vers son contenu (payload), tel que le contenu d'un fichier texte ou un enregistrement de base de données,
* des attributs sous forme de clé/valeur, qui représentent les métadonnées du FlowFile (ex : le nom du fichier),
* les événements de provenance (historique des modifications),

Chacun de ces composants dispose de son propre dépôt/stockage

Au niveau architecture, Apache NiFi se compose donc de plusieurs briques :
* Un **serveur web**, basé sur http et utilisé pour contrôler le logiciel et surveiller les événements,
* le contrôleur de flux (**Flow controller**) : le cerveau du logiciel qui contrôle l’exécution des extensions, la planification et l'allocation des ressources,
* les **extensions** : des modules qui permettent l’interaction avec les différents systèmes,
* le **FlowFile Repository** : le dépôt des FlowFiles et métadonnées associées,
* le **content Repository** : le dépôt des données en transit,
* le **provenance Repository** : le dépôt des provenances des données en transit.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Apache_NiFi_Components.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Apache_NiFi_Components.png)
<br/>
*fig 2 :Architecture Nifi*
&nbsp;
<br/><br/>
Apache Nifi est prévu pour fonctionner de manière distribuée, notamment en s'installant sur le framework **Hadoop** et en utilisant **HDFS** pour le stockage de ses données. Il est possible de ne pas utiliser HDFS, mais il faudra pour cela soit répliquer les données, soit utiliser un autre système stockage distribué.

Pour fonctionner en mode cluster, NiFi utilise Apache **Zookeeper** afin de gérer la synchronisation, la coordination et la configuration du cluster.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi-Cluster.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi-Cluster.png)
<br/>
*fig 3 : Nifi en mode cluster*
&nbsp;
<br/><br/>

En mode cluster, l'architecture distribuée, depuis la version 1.0, est basée sur un mode sans maître (**Zero Master Clustering**), chaque nœud du cluster effectue les mêmes tâches sur les données mais chacun opère sur des parties différentes du jeu de données. Dans l'idée on se rapproche de la notion de partition que l'on peut trouver en base de données ou dans des programmes tel que Spark.

Pour la coordination, **Zookeeper** élit un nœud du cluster comme coordinateur : **Le coordinateur** NiFi et un autre noeud comme nœud  principal ou **primary node** 

Le coordinateur est responsable de gérer l'ajout et la suppression de nœuds au cluster. Quand un nœud est ajouté, c'est lui qui coordonne l'affectation des tâches et la réplication des flux. Idem en cas de suppression (ou panne) d'un nœud, le coordinateur va réaffecter le jeu de données aux autres nœuds.

Le primary node quant à lui est un noeud sur lequel s’exécutent les tâches ou des processus que l'on ne peut ou ne veut pas distribuer (par exemple la connexion à une API ou à un FTP.)

A noter qu’en cas d’indisponibilité du coordinateurs, les autres nœuds continueront de fonctionner afin de maintenir la disponibilité des flux, mais aucun ajout ou modification du flux ne pourra se faire avant qu'il soit de nouveau disponible ou qun' nouveau ne soit élu. En cas de non disponibilité du primary node, une nouvelle élection a lieue et les tâches du nœud sont prises en charge par un autre nœud qui devient alors le primary node.

Le détail des nœuds et leurs fonctions sont documentés ici : [NiFi administration-guide](https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html#terminology "NiFi administration-guide")
&nbsp;

------------

### Cas d'usages et fonctionnalités

#### Principaux cas d'usages

Les cas d'usages sont nombreux, en effet NiFi peut être utilisé dès qu'il y a besoin de déplacer des données ou de gérer des flux de données entre systèmes différents. De par son support pour de nombreux protocoles, systèmes de stockage et formats de données, ainsi que son extensibilité, NiFi est un outil idéal pour l'ingestion de donnée de tout genre et/ou en volume.

Par exemple NiFi peut être utilisé :
* Pour l'ingestion de données vers un DataLake, depuis des sources tel que des API, des FTP ou encore des bases de données
* Pour l'alimentation de bases de données destinées à l'analyse (data Warehouse), un peu à la manière d'un ETL ([Extract, Transform & Load](https://fr.wikipedia.org/wiki/Extract-transform-load "Extract, Transform & Load")) ,
* Pour la gestion et la supervision de fichiers journaux, tel que du monitoring web,
* Comme outil de gestion et de supervision d'une architecture BigData (Lambda ou Kappa),
* Dans l'internet des objets (IoT), comme outil de collecte de données, voire de traitement

Pour ce dernier exemple, il est a noté qu'une version légère de NiFi a été développée spécifiquement pour ce cas d'usage : MiNiFi. Cette version, disponible en Java et en C++,  peut être installée sur des objets connectés ou des capteurs afin de collecter des données à la source et de les envoyer dans un flux NiFi.
&nbsp;

####  Description des fonctionnalités

Il serait quasi impossible de détailler tout ce que peut faire NiFi : il existe environ 200 composants que l'on peut relier entre eux pour former des flux et des applications. Le nombre de possibilités est immense.

Afin de quand même préciser ses possibilités, nous allons présenter les grandes "catégories" des composants de traitements de NiFi (les fameux processeurs).
&nbsp;

##### Principaux composants (processeurs) par catégories

- **Processeurs d'ingestion de données**
Les processeurs de cette catégories sont utilisés pour récupérer de la donnée dans le flux de données NiFi, ce sont généralement les composants de départ d'un flux. On peut citer par exemple GetFile, GetFTP, GetHTTP, GetKafka ou encore GetTwitter, ... Chaque nom étant explicite, on devine ce que récupère chacun.

- **Processeurs de routage ou d'arbitrage**
Ce sont des processeurs de contrôle qui vont permettre d'orienter le flux de donnée dans tel ou tel direction en fonction soit d'un attribut du flowFile, soit de son contenu. On a par exemple RouteOnAttribute, RouteOnContent, ou encore RouteText.

- **Processeurs d'accès à des bases de données**
Comme le nom l'indique, cette catégorie contient les processeurs capable de séléctionner ou écrire des données ou encore d'executer des requêtes dans une base de données. On peut citer par exemple : ListDatabaseTable, ExecuteSQL, PutDatabaseRecord, PutMongo, PutElasticsearchHttp ...

- **Processeurs d'extraction d'attributs**
Dans cette catégorie on regroupe les composants qui extraient, analysent ou changent les attributs du flowfile. On a par exemple UpdateAttribute, ExtractText, AttributesToJSON ou encore EvaluateJsonPath, ce dernier est par exemple capable d'extraire du contenu JSON d'un flowfile pour en faire un attribut, comme par exemple le timestamp d'un enregistrement.

- **Processeur d'intéraction avec le système**
Ces processeurs intéragissent avec le système pour executer des commandes ou un processus, ou alors encore lancer un script dans un des nombreux langages supportés (Python, Java, Scala, Lua, Javascript, ...). Dans cette catégorie, on a : ExecuteScript, ExecuteProcess, , ExecuteStreamCommand, etc...

- **Processeurs de transformation de données**
Dans cette catégorie on retrouve les processeurs qui vont transformer le contenu du flowFile. On y retrouve aussi les processeurs de conversion pour passer les données d'un format à un autre. On retrouve donc par exemple : ReplaceText, CSVToAvro, JoltTransformJSON, etc...

- **Processeur d'envoi de données**
Souvent, ce sont les processeurs de fin de flux (ou de branche du flux). Ces processeurs sont responsables de l'écriture ou de l'envoi de données vers un stockage, un serveur ou une application. En cas de succès du traitement ces processeurs terminent le flowFile, il n'y a pas de liaison sortante. Parmi cette catégorie on retrouve : PutEmail, PutKafka, PutSFTP, PutFile, PutHDFS, etc...

- **Processeur de divison ou d'aggrégation de données**
Ces processeurs servent à diviser ou au contraire fusionner le contenu des flowFiles. On a par exemple : SplitText, SplitJson, SplitXml, MergeContent, SplitContent, etc...

- **Processeurs HTTP**
Cette catégorie regroupe les processeurs responsables de gérer des requêtes HTTP/HTTPS ou des API. Par exemple : InvokeHTTP, PostHTTP, ListenHTTP, etc...

- **Processeurs pour AWS et Azure**
Dans cette catégorie on a des processeurs permettant d'intéragir avec les services de ces fournisseurs de service cloud. On a par exemple : PutS3Object, FetchS3Object, FetchAzureBlobStorage, ListAzureBlobStorage, PutCloudWatchMetric, ...
&nbsp;

##### Relations, connexions et files d'attente

Dans un flux NiFi les FlowFiles passent d'un composant à un autre à travers des **connexions** qui sont validées par des notions de **relations**. Quand une connexion est créée, le développeur doit choisir une ou plusieurs relations du processeur source. Les relations possibles dépendent du type de processeur mais on peut citer par exemple :

* Succès : cette relation est activée quand le processeur a réussi sa tâche sur le FlowFile
* Échec : cette relation est activée en cas d'échec du traitement
* Introuvable : le contenu du flowFile n'est pas disponible ou introuvable
* permission refusée : problème de permission
* non match : pour des processeurs de types filtrage ou recherche, le contenu ou l'attribut recherché n'a pas été trouvé.

Il est important de noter que toutes les relations d'un processeurs doivent être affectées, soit à une connexion sortante, soit à une **terminaison**. Les terminaisons se configurent dans les propriétés du processeurs et indiquent au processeur de détruire le flowFile quand tel ou telle relation est activée.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/configure_processor.jpg)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/configure_processor.jpg)
<br/>
*fig 4 : Relation d'un processeur*
&nbsp;
<br/><br/>
Toute relation qui n'est pas affecté en terminaison d'un processeur est une connexion sortante vers un autre processeur. A chaque connexion est associée une **file d'attente**. Ces files d'attente peuvent gérer de grandes quantités de FlowFile et on peut à tout moment contrôler leur statut, les vider ou voir le contenu des FlowFiles de la file.
&nbsp;

##### Groupe de processus et modèles

Les flux, leurs connexions et les différents services et variables associés peuvent être regroupés dans un groupe de processus (**process group**) à des fins de classement logique et/ou hiérarchique. Un groupe peut contenir d'autres groupes, des fluxs complets ou une combinaison des deux.

Les groupes sont à voir comme des unités organisationnelles, unités que l'on peut contrôler, piloter et pour lesquels ont peut définir des accès et autorisations. L'interface de NiFi permet de descendre ou de monter dans la hiérarchie de groupe, un peu à la manière de l'exploration d'une arborescence de fichiers.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_Processgroup.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_Processgroup.png)
<br/>
*fig 5 : 3 groupes de processus*
&nbsp;
<br/><br/>
NiFi offre aussi la possibilité d'enregistrer un flux, un groupe ou un ensemble de groupes et flux vers des modèles (**template**). Ces modèles peuvent ensuite être enregistrés au format xml permettant ainsi de sauvegarder ou de distribuer des flux et des applications complètes.

Une bonne pratique concernant les modèles est de mettre dans un groupe ce que l'on veut exporter (un flux ou un ensemble de flux/de groupes) et d'exporter le groupe résultant. En effet, un groupe comprend tous les flux, variables et services qui le composent et tout est donc exporté. Si on n'exporte que le flux actuel, il faudra alors recréer ses variables et services associés.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_templates.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_templates.png)
<br/>
*fig 6 : le gestionnaire de modèles avec les 3 groupes de processus exportés*
&nbsp;
<br/><br/>

##### Interaction avec d'autres outils de gestion de données et du BigData

Comme nous l'avons vu, certains composants permettent d’interagir avec d'autres logiciels. Parmi ceux-ci, certains peuvent même offrir une intégration assez avancée avec une application ou un prototocle afin de permettre des possibilités de traitement BigData complet. On peut notamment souligner l'importance de l'intégration de NiFi avec **Kafka**, **Spark**, **Hbase** ou encore **Spark Streaming**.

D'ailleurs, l'intégration avec Kafka est particulièrement notable. Kafka est un système distribué et hautement performant de gestion de file d'attente de messages, très largement utilisé dans les architectures BigData. NiFi étant lui un outil de gestion et d'ingestion de données, il semble assez naturel pour NiFi de pouvoir lire (consommer) ou écrire (produire) de la donnée vers les files d'attente de Kafka.

Vous trouverez plus d'informations sur Kafka sur le [le site officiel](https://kafka.apache.org/).
&nbsp;

##### Suivi des modifications (Data Provenance)

Une des autres grosses fonctionnalités de NiFi est le suivi complet des événements et des modifications associés à un FlowFile et son contenu. Ces informations sont stockées dans le **Data Provenance Repository** et peuvent être consultées à tout moment.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_dataprovenance.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_dataprovenance.png)
<br/>
*fig 7 : Tableau de suivi des modifications de données (data provenance)*
&nbsp;
<br/><br/>

En cliquant sur l'icône information au début d'une ligne, on a accès aux informations détaillées de l’étape avec les métadonnées, les valeurs d'attributs ou encore les modifications de contenus du FlowFile. On peut même consulter les données avant et après traitements par le processeur :

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_dataprovenance_content.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_dataprovenance_content.png)
<br/>
*fig 8 : Visualisation des modifications de contenu*
&nbsp;
<br/><br/>
Par défaut, NiFi conserve pendant 24h et à hauteur de 1Go les données de data Provenance, mais ces paramètres sont configurables dans le fichier de configuration **nifi.properties**
&nbsp;

------------

### Réalisation d'un simple flux 

Pour la réalisation de notre premier flux, on va prendre les fichiers qui "arrivent" dans un répertoire, modifier le contenu puis les écrire dans un répertoire cible.

#### GetFile : récupération d'un fichier

* On commence en ajoutant un processeur, en faisant un glisser/déposer sur l'icône vers la zone de travail

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_processor.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_processor.png)
    <br/>
    *fig 9 : Ajout d'un processeur*
&nbsp;<br/>

* On choisit le composant **GetFile** parmi les nombreux processeurs disponibles

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_processorGetFiles.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_processorGetFiles.png)
    <br/>
    *fig 10 : Liste des processeurs*
&nbsp;<br/>

* On peut double cliquer sur le processeur ainsi créé sur notre canvas pour éditer ses propriétés

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_GetFilesProperties.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_GetFilesProperties.png)
    <br/>
    *fig 11 : Propriétés du GetFile*
&nbsp;<br/>

* On ajoute notre dossier source, mais plutôt que d'utiliser un chemin système (tout à fait possible), on va utiliser une **variable de flux** qu'on va appeller *source.dir*. Ces variables sont liées au flux courant et permettent de contextualiser un flux. On verra comment les renseigner un peu plus loin

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_addVariables.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_addVariables.png)
    <br/>
    *fig 12 : Ajout d'une variable*
&nbsp;<br/>

#### Modification des données : ReplaceText

* On ajoute maintenant un processeur **ReplaceText** et on lui indique dans ses propriétés de remplacer "test" par "ceci est le test numéro"

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_ReplaceProperties.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_ReplaceProperties.png)
    <br/>
    *fig 13 : Propriétés du ReplaceText*
&nbsp;<br/>

* Par contre on va indiquer dans les paramètres (**settings**) qu'en cas d'échec on arrête le traitement du flowFile courant en cochant la case failure.

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_ReplaceSettings.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_ReplaceSettings.png)
    <br/>
    *fig 14 : Paramètres du ReplaceText*
&nbsp;<br/>

#### Ecriture des données : PutFile

* On ajoute un processeur **PutFile** et comme pour le **GetFile** on va utiliser une variable pour indiquer le répertoire. Dans les paramètres on indique que le composant doit se terminer en cas d'échec ou de succès.

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_PutFilesProperties.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/nifi_PutFilesProperties.png)
    <br/>
    *fig 15 : Propriétés du PutFile*
&nbsp;<br/>

* Il ne reste plus qu'à relier nos processeurs en cliquant sur l'icône flèche d'un composant source et en le glissant sur sa cible afin d'obtenir le flux suivant :

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/first-flow.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/first-flow.png)
    <br/>
    *fig 16 : Notre premier flux au complet*
&nbsp;<br/>

#### Ajout de variable et lancement du flux

* Avant de pouvoir démarrer notre flux, il faut maintenant renseigner nos variables de flux, pour cela on clique droit sur le canevas, dans une partie vide et on choisit **variables**

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_contextmenu.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/NiFi_contextmenu.png)
    <br/>
    *fig 17 : Menu contextuel (canvas)*
&nbsp;<br/>

* On ajoute nos deux variables *source.dir* et *dest.dir* avec comme valeur un répertoire différent pour chacune (ex : /home/dev/nifi/source et /home/dev/source/dest)

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Variables-Nifi.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Variables-Nifi.png)
    <br/>
    *fig 18 : Ajout de Variables au flux*
&nbsp;<br/>

* Notre Flux est prêt il ne reste plus qu'à le lancer, là aussi en utilisant le menu contextuel sur une zone vide, puis en faisant **start**

* Un petit test rapide en faisant un **echo** vers des fichiers dans notre répertoire source permet de vérifier que tout fonctionne :

    [![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/first-flow-result.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/first-flow-result.png)
    <br/>
    *fig 19 : Tests et résultats*
&nbsp;<br/>

------------

### Gestion des accès et des autorisations

#### Sécurisation de NiFi

Pour activer la sécurisation de NiFi, c'est à dire la gestion de l'identité des accès (authentification) et le contrôles des droits d'accès (autorisations) il faut activer le mode HTTPS dans la configuration de NiFi.

Une fois le HTTPS activé, NiFi va automatiquement se sécuriser et activer la gestion de l'authentification et des autorisations.
&nbsp;

#### Authentification

Par défaut NiFi est configuré pour utiliser l'authentification par certificat client SSL. NiFi intègre d'ailleurs une autorité de certification pour cet effet, ainsi que des outils permettant la génération de certificats simplement (TLS Toolkit), mais il est tout à fait possible de se servir d'une autorité tierce.

NiFi supporte également des fournisseurs d'identités tel que :

* LDAP,
* Kerberos,
* openID,
* Apache Knox, qui est, entre autres, une passerelle d'identification pouvant se connecter à une multitude de fournisseurs d'identités.

Une fois un utilisateur accepté, NiFi récupère son **distinguished name** (DN) pour l'identifier et gérer ses autorisations
&nbsp;

#### Gestion des autorisations

La gestion des autorisations de NiFi est multiple entités. Elle est basée sur des entités ou groupes (tenants) auquel sont associées des politiques d'accès.

Par défaut NiFi est configuré pour utiliser une gestion des groupes sous forme de fichiers mais il est tout à fait possible d’utiliser d'autres fournisseurs tel que : 
* Les groupes de LDAP,
* les groupes Shell (les groupe du système linux).

Pour la gestion des politiques d'accès, NiFi utilise en standard son propre système, là encore enregistrés sous forme de fichiers mais il est possible d'utiliser Apache Ranger à cet effet. Apache ranger est un framework de gestion d'autorisations pour les logiciels de l’écosystème Hadoop.

Les politiques d'accès peuvent être affichées et/ou configurées dans l'interface de NiFi (suivant les choix de fournisseurs de groupes/politiques) et son totalement modulaires car définit à deux niveaux :

* au niveau global : Accès aux écrans, interfaces, et configuration de NiFi
* au niveaux des composants, groupes ou connexions : qui peut accéder, voir, modifier, auditer, interroger, ou opérer.

Par défaut les autorisations sont héritées de leur parents (un composant hérite des autorisations définit sur son parent) mais cet héritage peut être cassé individuellement afin de définir une politique spéciale pour un composant ou groupe.
&nbsp;

------------

### Monitoring et statistiques de NiFi

NiFi permet de surveiller les différentes statistiques du système et de l’exécution des flux en temps réel : erreurs de traitement, charge mémoire et cpu, nombre et taille des données, etc.

A cet effet on peut citer notamment 3 interfaces/outils inclus de base : l'interface **summary**, l'interface **bulletin board**, l'interface des **data provenance**

Toutes ses interfaces sont accessibles par le menu de NiFi (icône en haut à droite)

Détaillons un peu ces interfaces :


#### L'interface Summary

Dans cette interface, on peut accéder aux informations et statistiques de tous les composants (processeurs) d'une instance de NiFi ou d'un cluster, ainsi qu'aux statistiques et détails des connexions, des groupes de processus ou encore des ports d'entrée et sortie (ce sont des ports qui permettent de passer des données d'un groupe de processus à un autre).

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_summary.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_summary.png)
<br/>
*fig 20 : NiFi Summary*
&nbsp;<br/>

#### L'interface bulletin board

Dans cette interface on a accès en temps réels aux **erreurs** et **warning** générés par les processeurs et connexions. En cliquant sur une erreur on peut accéder directement au processeur qui l'a générée.

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_bulletin.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/Nifi_bulletin.png)
<br/>
*fig 21 : NiFi Bulletin board*
&nbsp;<br/>

#### L'interface data provenance

Présentée un peu plus haut, elle permet de suivre les évènements de modifications d'attributs des FlowFiles ou les modifications intervenus sur les données.


#### et ...

NiFi embarque aussi des services dédiés au reporting (**reporting Task**) afin d'envoyer les statistiques, indicateurs ou messages vers un système de supervision.

Enfin NiFi met à disposition une API **system-diagnostics** qui peut être interrogée par un système de supervision.
&nbsp;

------------

### Forces et Faiblesses de NiFi

Pour résumer un peu les forces et faiblesses de NiFi, nous allons nous concentrer sur le rôle premier de NiFi : ingérer ou déplacer de la donnée en masse.

Dans cette optique, les principales forces de NiFi sont :

* Un concept simple et puissant à travers l’implémentation de la programmation basée sur les flux, qui offre un réel outil de création **wysiwyg** (What You See Is What You Get) de flux de données.

* Le nombre de format de données, systèmes et protocoles différents supportés : de par le nombre de processeur inclus en standard mais aussi la possibilité de programmer ses propres processeurs.

* Le suivi de modifications des données (**Data Provenance**) qui permet de suivre en temps réel et d'enregistrer toutes les transformations de données et événements du flux.

* NiFi est open source.

* Les performances et la possibilité de travailler en temps réel.

* Une gestion des droits d'accès et des autorisations multi-entités très puissante et s'intégrant avec des standards reconnus.

A contrario, on peut citer quelques faiblesses : 

* Une interface utilisateur un peu spartiate, elle fait le boulot et est simple à prendre en main mais ça manque un peu de couleur, de possibilité "responsive" et de personnalisation.

* L'absence de supervision/débogage au niveau d'un enregistrement : toutes ces fonctionnalités existent au niveau du des processeurs, connexions ou du FlowFile mais on ne peut pas descendre au niveau enregistrement.

* L'absence de fonction de jointure : sans doute la plus grosse faiblesse de NiFi face à des solutions de types ETL, comme on le verra ci-dessous. NiFi peut fusionner des flux mais uniquement de manière additive (agrégation), il ne permet pas de croisement de données et de filtrage par jointure externe ou interne.
&nbsp;

------------

### Solutions alternatives

#### Solutions concurrentes

La principale alternative à NiFi, en open source, est [Streamset](https://streamsets.com/)

Par rapport à NiFi, StreamSets offre une interface graphique évoluée permettant plus de personnalisation et un suivi des statistiques en temps réel facilité.

A l'inverse il offre moins de connecteurs et de supports pour des données binaires et ne permet pas d'intervenir sur les connexions ou les composants sans arrêter le flux au complet.

StreamSet ne permet pas non plus la mutualisation d'éléments de configurations ou de services entre différents processeurs, chaque processeur doit être configuré indépendamment.

En solution propriétaire, dont certaines basées sur NiFi on peut citer :

* [Azure Data Factory (Microsoft)](https://azure.microsoft.com/en-us/services/data-factory/)
* [IBM InfoSphere DataStage](https://www.ibm.com/us-en/marketplace/datastage)
* [Data Pipeline (Amazon)](https://docs.aws.amazon.com/en_us/datapipeline/latest/DeveloperGuide/what-is-datapipeline.html)
* [Dataflow (google)](https://cloud.google.com/dataflow/)
&nbsp;

#### Solutions similaires

Si l'on ne considère que la partie ingestion de données, alors Apache Kafka couplé à un framework de traitement distribué pour l'écriture ([Hadoop](https://hadoop.apache.org/), [Spark](https://spark.apache.org/), ...) est plus performant que NiFi mais plus complexe à mettre en place car cela demande de la programmation.

A l'inverse si on s'intéresse uniquement à la partie traitement, des solutions tel que [Apache Storm](https://storm.apache.org/) pour le temps réel ou [Spark](https://spark.apache.org/) pour le traitement batch offrent plus de performances, mais ne sont pas à la portée de tous. Spark, comme mentionné dans les fonctionnalités, peut d'ailleurs être intégré à un Flux NiFi grâce à un processeur.

Si on ne considère que l'ingestion/traitement de **fichiers journaux**, comme des logs de serveurs web par exemple, des solutions spécialisées tels que [Apache Flume](https://flume.apache.org/) ou [LogStash](https://www.elastic.co/fr/logstash) sont aussi à considérer, mais encore une fois un peu plus complexes à mettre en place (langage de script/fichier de configuration)

Enfin, si les aspects temps réel et volume de données ne sont pas essentiels, ou pour le travail dans des environnements relationnels, les solutions de type ETL (Extract, Transform & Load) peuvent se substituer à NiFi, par exemple en open-source :

* [Talend Open Studio](https://fr.talend.com/products/talend-open-studio/)
* [Pentaho](https://community.hitachivantara.com/s/pentaho)

Il est à noter ici que bien que les ETL et NiFi peuvent se recouper sur certaines tâches, leurs cibles sont quelques peu différentes. Les ETL ne permettent pas de travailler sur des données non structurées et sont plutôt destinées au monde relationnel. NiFi ne permet pas de lier/joindre des jeux de données ou des tables et se destine plutôt au monde du BigData (ou à la liaison entre les deux mondes).
&nbsp;

#### Comparaison des outils dans le monde du BigData

Si on se concentre sur les enjeux actuels, à savoir le monde du BigData il est important de comparer les possibilités de chacun de ses outils à gérer les 3 V : Volume, Vélocité et Variété tout en les opposant à la complexité de mise en place (connaissance en programmation et temps de développement) et la complexité de supervision/maintenance de la solution.

Le tableau suivant présente une vision d'ensemble des qualités de chacun comparés aux autres :

[![](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/toolsComp.png)](https://gitlab.com/Djuleroux/daprojet5/-/raw/master/doc/img/toolsComp.png)
<br/>
*fig 22 : comparaison de NiFi avec d'autres solutions dans le monde du BigData*
&nbsp;<br/>

------------

### Conclusion : Place de NiFi dans un pipeline de données

Comme nous l'avons vu, dans un pipeline de données NiFi peut remplir bien des rôles autour du déplacement et du traitement des données.

Il est avant tout et historiquement un outil de création de processus de transfert de données de manière graphique, que ce soit dans le cadre d'une ingestion de données pour un datalake, d'un outil de transfert entre systèmes différents ou de l'intégration d'un flux temps réel dans une base NoSQL. Mais NiFi est bien plus que ça aujourd'hui...

Dans la litérature on oppose souvent :
* l'ETL (Extract,Transform & Load) du monde relationnel, ou les transformations ont lieu entre les passages de systèmes,
* l'ELT (Extract, Load and Transform) du monde BigData ou les transformations se font souvent en fin de chaine.

Dans ces schémas NiFi, NiFi est l'E et le L. Il peut assurer aussi des transformations mais ce n'est pas son rôle premier, ce qui fait notamment que NiFi est plus à l'aise dans les architectures BigData, c'est là ça principale différence avec les ETL traditionnels pour qui c'est l'inverse.

Dans tous les cas, même s'il n'est pas le champion des transformations, NiFi peut, en s'appuyant grâce à ses processeurs sur des solutions tierce, les piloter et surveiller leur déroulement, et à chaque nouvelle version le nombre de frameworks, systèmes ou solutions de transformations supportés augmente.

NiFi peut aussi piloter et surveiller les données jusqu'à leur stockage, servant alors d'outil de supervision de flux voire d'architecture. On pourrait ainsi dire, que NiFi est en quelque sorte le pilote des processus de traitement de données :

[![](https://dju-da.site/wp-content/uploads/2020/02/Nifi-dp.png)](https://dju-da.site/wp-content/uploads/2020/02/Nifi-dp.png)
<br/>
*fig 23 : NiFi, le pilote du data processing*
<br/>
Ce rôle lui sied à merveille et dans l'ecosystème des outils Apache pour le big data : de la gestion des messages (Kafka) au traitement (Spark, Storm) en passant par le stockage (Hbase), NiFi avec son interface graphique et son accessibilté a de quoi fortement séduire...

&nbsp;

------------

### Exemples d'utilisation

#### POC 1 : Analyse de twitter en temps réel avec NiFi
Vous le trouverez ici : [Analyse temps réels des tweets avec NiFi](https://dju-da.site/data-processing/analyse-temps-reel-des-tweets-avec-nifi/ "Analyse temps réels des tweets avec NiFi")
&nbsp;

#### POC 2 : Ingestion de données dans un dataLake

Vous le trouverez ici : [Créer son Datalake avec NiFi et HDFS](https://dju-da.site/data-processing/creer-son-datalake-avec-nifi-et-hdfs/ "Créer son Datalake avec NiFi et HDFS")

&nbsp;

------------

### Références & pointeurs

* Apache NiFi System Administrator guide : [Link](https://nifi.apache.org/docs/nifi-docs/html/administration-guide.html)
* Présentation, en Français de NiFi : [Blog Meritis](https://meritis.fr/bigdata/travailler-donnee-apache-nifi/)
* un Tutorial pour débuter avec NiFi : [Gentle Introduction to Apache NiFi](https://dzone.com/articles/gentle-introduction-to-apache-nifi-for-dataflow-an)
* Une description détaillé du fonctionnement de NiFi : [How Apache NiFi Works](https://www.freecodecamp.org/news/nifi-surf-on-your-dataflow-4f3343c50aa2/)
* Blog d'articles très intéressant sur NiFi (Sujets avancés) : [Pierre Villard's Blog](https://pierrevillard.com/)
* "Best Practices" pour les performances : [NiFI best practices for High performance](https://community.cloudera.com/t5/Community-Articles/HDF-NIFI-Best-practices-for-setting-up-a-high-performance/ta-p/244999)
* Une comparaison détaillé de NiFi vs Streamset : [link](https://statsbot.co/blog/open-source-etl/)
* La présentation de Renault sur NiFi lors du Hadoop Submit 2018 : [Running Apache NiFi at Renault](https://fr.slideshare.net/Hadoop_Summit/best-practices-and-lessons-learnt-from-running-apache-nifi-at-renault)
