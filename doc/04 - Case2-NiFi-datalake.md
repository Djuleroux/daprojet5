Dans cette dernière partie sur Apache NiFi, on va s'intéresser à un autre exemple de cas d'usage de Nifi. Nous allons ingérer automatiquement des données provenant de la [Plateforme "Transparency"](https://transparency.entsoe.eu/) du réseau européen des gestionnaires de réseau de transport d’électricité (ENTSOE).

Pour cela on va réaliser un flux NiFi d'ingestion de données qui va :
- se connecter à la plateforme en Sftp pour vérifier les fichiers ayant été changés et les récupérer (csv)
- transformer les csv en format de données sérialisés
- filtrer/transformer les données
- écrire les données sur un cluster HDFS

L'idée ici, après avoir présenter dans le cas d'usage précédent la création d'une speed Layer avec NiFi est de travailler plutôt sur la couche **"batch"** d'une architecture du type Lambda et de présenter un flux d'ingestion de données dans un DataLake (ici géré par HDFS). En bonus nous piloterons aussi un job d'analyse spark qui écrira dans mongodb les données agrégées depuis le datalake.

Le template NiFi et les résultats sont disponibles sur le dépôt Git du projet [ici](https://gitlab.com/Djuleroux/daprojet5), mais détaillons un peu la source, le flux NiFi et les composants utilisés :
&nbsp;

### Description des fichiers sources

Les fichiers de l'entsoe sont documentés et détaillés [ici](https://transparency.entsoe.eu/content/static_content/Static%20content/knowledge%20base/knowledge%20base.html). Dans cet exemple on va s'intéresser aux fichiers **Actual Generation per Generation Unit** dans lesquels on retrouve les générations électriques et les consommations électriques mesurées toutes les 15 minutes et ventillées par Usine de production, type de production, zones et pays.

Les données sont disponibles sur le SFTP sous forme de fichiers mensuels mis à jour tous les matins avec les données de la veille.

Voici les principaux champs du fichier qui nous intéresseront :
* **DateTime** : date et heure de la mesure sous forme de chaîne de caractères. ex : 2019-03-01 00:15:00.000
* **MapCode** : Pays ou sous division du pays sous forme de code ISO à 2 lettres + code. eg : FR
* **PowerSystemRessourceName** : usine de génération
* **ProductionTypeName** : type de production (ex : Nuclear)
* **ActualGenerationOutput**, ActualConsumption, InstalledGenCapacity : les mesures de production, consommation et capacité de production.

Au niveau technique, les fichiers sont au format UTF-16 avec une séparation des champs par tabulation.
&nbsp;

### Détail des composants du flux

#### ListSFTP et FetchSFTP

Le premier composant sert à lister les fichiers sur le SFTP distant suivant des conditions prédéterminés. Ici on va suivre les fichiers uniquement modifiés depuis moins de 32 jours et qui commence par 2019 ou 2020. Les autres paramètres concerne l'adresse du serveur et l'identification.

On programme ce composant pour tourner chaque jour

Le deuxième va récupérer les variables **path** et **filename** (ainsi que les informations de connexion) du premier composant afin de récupérer les fichiers.

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-listsftp1-700x488.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-listsftp1.png)
*fig 1 : le composant ListSftp*
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-listsftp2-700x175.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-listsftp2.png)
*fig 2 : suite des propriété du ListSftp*
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-fetch-sftp-700x495.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-fetch-sftp.png)
*fig 3 : le composant FetchSftp*
&nbsp;

L'intérêt de séparer les 2 opérations **list** et **fetch** plutôt que d'utiliser un processeur **GetSftp** se situe à deux niveaux :
* le ListSftp permet un filtrage plus fin ainsi qu'un suivi des fichiers dans le temps (taille, timestamp, nom...).

* La liste des fichiers obtenus du ListSftp peut ensuite servir au partitionnement des données afin de traiter le flux sur plusieurs nœuds.
&nbsp;

##### ConvertCSVToAvro

Ce composant sert à convertir les données du format CSV au format **Avro**. Avro est un format de sérialisation des données qui offre trois principaux avantages par rapport à un format texte standard :
* les données embarquent le schéma,
* les données sont stockées au format binaire,
* le format est indépendant et supporté par de nombreux langages.

Pour faire cette conversion on va fournir au processeur le schéma Avro souhaité en utilisant l'ordre et le nom de champs du fichier CSV. On utilise une variable pour cela.

Cela donne pour le début du fichier :

```json
{
  "type" : "record",
  "name" : "EntsoeData",
  "doc" : "Entsoe GenerationOutput per unit schema",
  "fields" : [ {
    "name" : "Year",
    "type" : "long",
    "doc" : "year of measure"
  }, {
    "name" : "Month",
    "type" : "long",
    "doc" : "Month of measure"
  }, {
    "name" : "Day",
    "type" : "long",
    "doc" : "Day of measure"
  }, {
    "name" : "DateTime",
    "type" : "string",
    "doc" : "measure time"
  }, {
    "name" : "ResolutionCode",
    "type" : "string",
    "doc" : "Resolution Code"
  }, [...]

```
&nbsp;

Le reste des propriétés du processeur sont les infos du format CSV :

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-CsvToAvro-700x430.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-CsvToAvro.png)
*fig 4 : propriété du CsvToAvro*
&nbsp;

#### UpdateRecords

Ce processeur va nous permettre de modifier les enregistrements du FlowFile. Ce processeur repose sur deux **services** : un Record Reader et un Record Writer. On utilisera ici un **AvroReader** et un **AvroSetWriter**.

Pour le Reader on le configure avec l'option du schéma embarqué (dans notre précédent processeur on a embarqué le schéma avec nos données)

Pour le Writer on va utiliser un schéma de données différent, là encore à l'aide d'une variable. Dans ce schéma on ne va retenir que les champs qui nous intéressent. Le champ **date**, de type long, est un nouveau champ, on le calculera à partir du champ **DateTime** original :

```json
{
  "type" : "record",
  "name" : "EntsoeData",
  "doc" : "Schema generated by Kite, modified by Dju",
  "fields" : [ {
    "name" : "Date",
    "type" : "long",
    "doc" : "Measurement date"
  }, {
    "name" : "AreaName",
    "type" : "string",
    "doc" : "Area Name"
  }, {
    "name" : "MapCode",
    "type" : "string",
    "doc" : "Map Code"
  }, {
    "name" : "PowerSystemResourceName",
    "type" : "string",
    "doc" : "power System Resource Name"
  }, {
    "name" : "ProductionTypeName",
    "type" : "string",
    "doc" : "production Type name"
  }, {
    "name" : "ActualGenerationOutput",
    "type" : [ "null", "double" ],
    "doc" : "Generation",
    "default" : null
  }, {
    "name" : "ActualConsumption",
    "type" : [ "null", "double" ],
    "doc" : "Consumption",
    "default" : null
  }, {
    "name" : "InstalledGenCapacity",
    "type" : [ "null", "double" ],
    "doc" : "Capacity",
    "default" : null
  } ]
}
```
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-avroreader-service-700x325.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-avroreader-service.png)
*fig 5 : service AvroReader*
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-avrowriter-service-700x413.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-avrowriter-service.png)
*fig 6 : service AvroWriter*
&nbsp;

On indique aussi au processeur que le champs date est une transformation du champs DateTime.

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-updateRecord-700x290.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-updateRecord.png)
*fig 7 : propriétés du UpdateRecords*
&nbsp;

#### UpdateAttributes

Ici, on utilise juste ce processeur afin de renommer le fichier de xxx.csv à xxx.avro

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-updateAtt-700x475.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-updateAtt.png)
*fig 8 : propriétés du UpdateAttributes*
&nbsp;

#### PutHdfs

Ce dernier composant nous permet d'écrire nos données dans HDFS. On utilise une politique "replace" de gestion des fichiers existants afin de gérér le fichier du mois courant qui est mis à jours chaque jour.

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-putHdfs-700x500.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-putHdfs.png)
*fig 8 : propriétés du PutHdfs*
&nbsp;

### Flux complet

Avant de relier nos différents processeurs on n'oublie pas de configurer les variables du flux avec nos schémas :

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-vars-700x247.png)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-vars.png)
*fig 9 : Définition des variables du flux*
&nbsp;

Voici notre flux complet avec les connexions :

[![](https://dju-da.site/wp-content/uploads/2020/01/datalake-full-700x437.png)](https://dju-da.site/wp-content/uploads/2020/01/datalake-full.png)
*fig 10 : Notre flux d'ingestion complet*
&nbsp;

### Résultats

Voici un extrait du contenu inscrit dans mongoDB depuis Spark :

[![](https://dju-da.site/wp-content/uploads/2020/01/entsoe-sparkread-700x199.jpg)](https://dju-da.site/wp-content/uploads/2020/01/entsoe-sparkread.jpg)
*fig 11 : Contenu du DataFrame Spark écrit dans Mongo*
&nbsp;

Pour donner plus de sens à nos données, réalisons une petite requête sur mongoDB. On va sélectionner les plus gros producteurs par type de production sur le mois de décembre 2019. La requête donne :
```js
//définition variables

varDateDebut = "2019-12-01T00:00:00Z"
varDateFin = "2019-12-31T23:59:59Z"

//on fixe les params des query pour réutilisation
varMatch = { $match : {"Date":{$gte: new ISODate(varDateDebut)*1,$lt: new ISODate(varDateFin)*1}}};
varGroup = { $group : {"_id" : {Pays:"$MapCode",Type:"$ProductionTypeName"}, "production" : {$sum : "$sum(ActualGenerationOutput)"} } };
varSort = { $sort : {"production":-1} };
varLimit = { $limit : 50 }

//query code
db = connect("localhost/db");
cursor = db.entsoe.aggregate( [ varMatch,varGroup,varSort, varLimit] );

while ( cursor.hasNext() ) {
    printjson( cursor.next() );
 }
```
&nbsp;

Ce qui nous donne comme résultat :

```json
{
        "_id" : {
                "Pays" : "FR",
                "Type" : "Nuclear"
        },
        "production" : 33172797
}
{
        "_id" : {
                "Pays" : "GB",
                "Type" : "Fossil Gas"
        },
        "production" : 17367243.58
}
{
        "_id" : {
                "Pays" : "GB",
                "Type" : "Nuclear"
        },
        "production" : 9738702.92
}
{
        "_id" : {
                "Pays" : "IT",
                "Type" : "Fossil Gas"
        },
        "production" : 7708762
}
{
        "_id" : {
                "Pays" : "GB",
                "Type" : "Wind Offshore"
        },
        "production" : 6342416.84
}
```
&nbsp;

On peut noter la première position de la France... avec le nucléaire (sans commentaire ;))
Notons aussi la production éolienne maritime de la Grande Bretagne, 5e.
&nbsp;
