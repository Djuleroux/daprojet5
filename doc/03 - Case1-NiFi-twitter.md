Dans ce troisième article sur la solution Apache NiFi de gestion de flux de données, nous allons nous intéresser à un cas d'usage "réel" : le traitement temps réél du flux de messages Twitter. On va pour cela, en quelque sorte, reproduire une "speed layer" complète d'une architecture BigData de type Lambda.

A cette fin on va :
* Se connecter à l'API de streaming **Twitter** afin de mettre dans une file d'attente (ici interne à NiFi) les tweets
* filtrer les tweets et attributs qui nous intéressent en temps réel
* injecter les données dans une base NoSQL, ici **ElasticSearch**.

En bonus on affichera les données dans un tableau de bord **Kibana**.

Le template NiFi et les résultats sont disponibles sur le dépôt Git du projet [ici](https://gitlab.com/Djuleroux/daprojet5). Mais détaillons un peu la source de données, le flux NiFi et les composants utilisés pour notre architecture :
&nbsp;

### Description de la Source

L'API Twitter expose plusieurs flux de Twitter en temps réel. On va utiliser dans cet exemple l'API **sample** qui est une API en accès libre permettant de capter en temps réel environs 1% des tweets. Vous trouverez l'ensemble de la documentation sur le site [Twitter Developer](https://developer.twitter.com/en/docs/tweets/sample-realtime/overview/GET_statuse_sample "Twitter Developer")

Pour pouvoir utiliser l'API, il faut créer et enregistrer un compte sur le site, puis inscrire son application pour obtenir un couple clefs d'accès et jeton (Consumer et Token).

Cette API va nous fournir les tweets et ses métadonnées associées sous forme de fichiers **json**, dont voici un extrait :
```json
{
    "created_at" : "Mon Jan 20 19:01:53 +0000 2020",
    "id" : 1219334248600981509,
    "id_str" : "1219334248600981509",
    "text" : "RT @SampurnaSinha1: #HeroicAsim         \n\nUncivilised                Educated https://t.co/NqPQm6kj0S",
    "source" : "<a href=\"http://twitter.com/download/android\" rel=\"nofollow\">Twitter for Android</a>",
    "truncated" : false,
    "in_reply_to_status_id" : null,
    "in_reply_to_status_id_str" : null,
    "in_reply_to_user_id" : null,
    "in_reply_to_user_id_str" : null,
    "in_reply_to_screen_name" : null,
    "user" : {
      "id" : 1201096197055770624,
      "id_str" : "1201096197055770624",
      "name" : "Ali",
      "screen_name" : "Ali71739585",
      "location" : null,
      "url" : null,
      "description" : null,
	  [...]
```
&nbsp;

Dans cet exemple, nous nous intéresserons uniquement à la date/heure de création du tweet ainsi qu'aux **hashtags** qui le composent.
&nbsp;

### Description des composants

#### GetTwitter

Ce composant permet de se connecter à une des API de streaming de twitter pour récupérer les données. Comme vu, nous allons utiliser ici l'API **"sample"** pour récupérer le contenu des tweets. Chaque tweet sera donc un message de type flowfile ayant pour contenu du texte au format JSON.

Le composant est simple à paramétrer et seul sont obligatoires les clés/tokens de votre compte (dev) twitter. Au niveau des paramètres, on va mettre le **"schedule"** à 0 sec afin de rester connecté en permanence à l'API et de récupérer tous les messages.

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-gettweet.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-gettweet.png)
*fig 1 :propriétés de GetTwitter*
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-gettweet2-e1580066019639.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-gettweet2-e1580066019639.png)
*fig 2 : orchestration de GetTwitter*
&nbsp;

#### EvaluateJsonPath et RouteOnAttribute

Les deux composants suivants vont nous servir à filtrer nos tweets :

* Le premier va transformer un ou plusieurs champs du json du tweet en attribut utilisable par NiFi. Ici on va prendre le texte du premier hashtag et le timestamp du tweet. On les affecte aux variables **twitter.hashtags** et **twitter.time**.

* Le second est le filtre en tant que tel, il permet de supprimer du flux tous les FlowFiles dont l'attribut hashtag est vide, c'est à dire tous les tweets qui n'ont pas au moins un hashtag. On utilise pour cela la stratégie **Route to property name** et on ajoute une ligne pour affecter à la propriété tweet la valeur de notre variable **twitter.hashtags**.

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-jsonpath.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-jsonpath.png)
*fig 3 :propriétés de EvaluateJsonPath*
&nbsp;

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-routeOnAttrib.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-routeOnAttrib.png)
*fig 4 :propriétés de RouteOnAttribute*
&nbsp;

Pour chacun de ces deux flux, on choisit de terminer le FlowFile en cas de non correspondance ou d'échec de conversion (Failure et unmatched), ce qui permet de ne passer dans la relation tweet de notre flux que les tweets qui ont au moins un hashtag.
&nbsp;

#### JoltTransformJSON

Ce composant sert à transformer le json entre l'entrée et la sortie en utilisant la librairie JOLT (JsON Language for Transform).

On s'en sert ici afin de ne laisser dans le json que les champs **"timestamp_ms"** et **"entities.hashtags.text"** (transformer en liste) que l'on renomme respectivement en **"date"** et **"hashtags"**

Voici le code à mettre dans "propriété/Jolt spécification" :
```
[
  {
    "operation": "shift",
    "spec": {
      "timestamp_ms": "date",
      "entities": {
        "hashtags": {
          "*": {
            "text": "hashtags"
          }
        }
      }
    }
  }
]
```
&nbsp;

#### PutElasticSearchHttp

Le dernier composant, comme son nom l'indique, est responsable de l'écriture sur le socket http de **ElasticSearch**.

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-PutElasticSearchHttp.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-PutElasticSearchHttp.png)
*fig 23 :propriétés de PutElasticSearchHttp*

Pour ce dernier composant on indique que l'on veut supprimer le FlowFile après un succès (écriture faite), on lui rajoute également un lien sur lui même que l'on paramètre sur "retry" et "failure" afin de retenter d'écrire les données en cas d'échec.
&nbsp;

### Flux complet et résultats

Voici le flux complet démarré :

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-full-1024x658.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-full.png)
*fig 5 : Flux de données Twitter*
&nbsp;

Le flux semble capable de gérer les 4000 à 5000 tweets par minute de l'API sample sans difficulté, les charges mémoires et processeurs restent faibles :

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-top.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-top.png)
*fig 6 : impact processeur et mémoire (1ere ligne)*
&nbsp;

On vérifie que les données sont bien écrite dans ES :

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-writeES.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-writeES.png)
*fig 7 : écriture dans ES*
&nbsp;

Afin de compléter notre speed layer avec une serving layer, et compléter notre architecture Temps réél on réalise un petit tableau de bord sous Kibana :

[![](https://dju-da.site/wp-content/uploads/2020/01/twitter-Kibana-1024x436.png)](https://dju-da.site/wp-content/uploads/2020/01/twitter-Kibana.png)
*fig 8 : hashtags en temps réel*
&nbsp;
