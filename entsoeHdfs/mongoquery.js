//définition variables

varDateDebut = "2019-12-01T00:00:00Z"
varDateFin = "2019-12-31T23:59:59Z"

//on fixe les params des query pour réutilisation
varMatch = { $match : {"Date":{$gte: new ISODate(varDateDebut)*1,$lt: new ISODate(varDateFin)*1}}};
varGroup = { $group : {"_id" : {Pays:"$MapCode",Type:"$ProductionTypeName"}, "production" : {$sum : "$sum(ActualGenerationOutput)"} } };
varSort = { $sort : {"production":-1} };
varLimit = { $limit : 50 }

//query code
db = connect("localhost/db");
cursor = db.entsoe.aggregate( [ varMatch,varGroup,varSort, varLimit] );

while ( cursor.hasNext() ) {
    printjson( cursor.next() );
 }
