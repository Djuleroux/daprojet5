#!/bin/sh

## Entsoe files analysis
time spark-submit --packages org.apache.spark:spark-avro_2.11:2.4.3,org.mongodb.spark:mongo-spark-connector_2.11:2.4.1 ./sparkAnalysis.py
