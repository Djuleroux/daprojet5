import os
import math
from pyspark import SparkContext
from pyspark.sql import SparkSession, Row
from pyspark.sql import functions as F
from pyspark.sql.types import *
import sys
import json

hdfs_host = "hdfs://localhost:9000"

# Mongo setup
spark = SparkSession.builder\
   .config("spark.mongodb.input.uri", "mongodb://127.0.0.1/db.entsoe")\
   .config("spark.mongodb.output.uri", "mongodb://127.0.0.1/db.entsoe")\
   .getOrCreate()

src_path = hdfs_host + "/data/entsoe/master"

# read avro files
datasrc = spark.read.format("avro").load(src_path + "/*.avro")

# Analysis
datacomp = datasrc.withColumn("Date",datasrc.Date - (datasrc.Date % 3600000))\
    .groupBy("Date","AreaName","MapCode","PowerSystemResourceName","ProductionTypeName").sum("ActualGenerationOutput","ActualConsumption","InstalledGenCapacity")\
    .withColumn("_id",F.concat("Date","PowerSystemResourceName","ProductionTypeName")).cache()

datacomp.show()
# write to mongo
datacomp.write.format("mongo").mode("append").save()
