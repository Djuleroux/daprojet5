# Projet 6 Data Architect - OC : Veille informatique

## Project's goals:

 * Technology intelligence on a Big Data or data processing tool
 * presentation video and writed reports about the tool, its use case and one or two test case scenario.

 Apache Nifi Was chosen for this work.
 
 **Important :** The reports and test cases are published in a friendly format on my blog here : [https://dju-da.site/](https://dju-da.site/), here on gitlab you will find a copy of the blog's texts and the sourcefile of the article's test cases
 

## Included

**doc :**
* the writed reports in md format, copy from my blog articles, (French)
* img : a copy off all assets used in the report (pictures, illustrations, charts)

**exemple1 :**
* a simple example of a working NiFi flow. (NiFi template in xml)

**twitter :**
* twitter xml : Nifi template of the first test case. This NiFi flow takes data from the twitter API (sample one by default) and inject the DATA in elasticSearh in realtime
* es.http : helper file to create and manipulate Elasticsearch index.
* results : screenshots of the job running and the Kibana dashboard

**entsoeHdfs :**
* EntsoeToHdfs.xml : NiFi template of the second use case. This NiFi flow ingest changed data from the Entsoe sftp (Electrical european production per plant and per 15min) into and HDFS DataLake.
* Schema.avro and schemaout.avro : input and ouput schema of our data
* sparkanalysis ana analyze.sh : spark script to aggregate data from the datalake into hourly synthetic data and write them to a mongo index and its launcher.
* mongoquery.js :  javascript script for querying mongo Index and analyze data into indicators.
* FileExample.csv : small extract of a source file

## Prerequisites

To run the test case you will need :

* java 8 at least
* a linux or docker host server to install and run apache NiFi
* a working HDFS cluster
* a working instance of Elastic Search


## Usage :

NiFi XML template can be imported in NiFi from the template management tool.

All templates links to localhost and default port for all connections (HDFS, ElasticSearch, Kafka...) change the NiFi processor properties accordingly.

For Twitter, you also need to put in your consumer and token informations in the GetTwitter processor.


## Architecture

see blog or doc

## Performances and results :

see blog or doc


## todo list :

Use NiFi @ work and publish more article on the blog ;)
